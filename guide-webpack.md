# Webpack

Made by @kevin.wolff

## Get started

Make sur npm is globaly install on your system, if not [npm website](https://www.npmjs.com/get-npm)

```shell
node -v
```

```shell
npm -v
```

**Init npm** in your project

```shell
npm init
```

**Install webpack**

```shell
npm install webpack webpack-cli --save-dev
```

**Setup requirements**

You need to setup some folders depend on your configuration, here we use sass

```shell
touch webpack.config.js
mkdir assets
mkdir assets/js
mkdir assets/sass
touch assets/js/app.js
touch assets/sass/app.scss
```

## Good to know

**Webpack watch** :
To make webpack compile your files you need to run this command line

```shell
npx webpack --watch
```

**stylesheet and script html links **:
In the following configurations webpack'll at least compile javascript files in *public/app.js*. If webpack also compile your css or sass files they'll be build in public/app.css

**Git repository ?** :
If you use git you probably don't want to push your node_modules folder, it contains thousands of files. Remember to add it in your .gitignore. There is still package.json and package-lock.json who allow to restore the node_module with this command line

```shell
npm install
```

**Development and production mode** :
You can switch between development and production mode in *webpack.config.js*, production mode will minify your file

## Configurations

### #1 only javascript

Webpack compile your *assets/js/app.js* in *public/app.js*

**webpack.config.js**

```js
const path = require('path');

module.exports = {
	mode: 'development',
	entry: {
		app: \"./assets/js/app.js\"
	},
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, 'public')
	},
};
```

### #2 Javascript and css / sass

Same as configuration #1 + compile css / sass file throught javascript when import css / sass file in *assets/js/app.js*

```js
import '../sass/app.scss'
```

**Required packages**
*see packages chapter for command lines*

- [Css-loader](https://webpack.js.org/loaders/css-loader/)
- [Sass-loader + Sass](https://webpack.js.org/loaders/sass-loader/)
- [Style-loader](https://webpack.js.org/loaders/style-loader/)
- [Mini-css-extract-plugin](https://webpack.js.org/plugins/mini-css-extract-plugin/)

**webpack.config.js**

```js
const path = require('path');
	const MiniCssExtractPlugin = require('mini-css-extract-plugin');

	module.exports = {
		mode: 'development',
		// mode: 'production',
		entry: {
			app: "./assets/js/app.js"
		},
		output: {
			filename: '[name].js',
			path: path.resolve(__dirname, 'public')
		},
	  	plugins: [
		  	new MiniCssExtractPlugin({
		  		filename: '[name].css'
		  	})
	  	],
		module: {
			rules: [
				{
					test: /\.css$/,
					use: [
						'style-loader',
						'css-loader'
					]
				},
				{
					test: /\.scss$/,
					use: [
						MiniCssExtractPlugin.loader,
						'css-loader',
						'sass-loader',
					]
				}
			]
		}
	}
```

### #3 Javascript + Babel and css / sass

Same as configuration #1 and #2 + Babel will allow you to write in the latest version of javascript and compile those features down to a supported version

**Required packages**
*see packages chapter for command lines*

- [Css-loader](https://webpack.js.org/loaders/css-loader/)
- [Sass-loader + Sass](https://webpack.js.org/loaders/sass-loader/)
- [Style-loader](https://webpack.js.org/loaders/style-loader/)
- [Mini-css-extract-plugin](https://webpack.js.org/plugins/mini-css-extract-plugin/)
- [Babel loader](https://github.com/babel/babel)

**webpack.config.js**

```js
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
	mode: 'development',
	// mode: 'production',
	entry: {
		index: "./src/js/index.js"
	},
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, 'public')
	},
  	plugins: [
	  	new MiniCssExtractPlugin({
	  		filename: '[name].css'
	  	})
  	],
	module: {
		rules: [
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env']
					}
				}
			},
			{
				test: /\.css$/,
				use: [
					'style-loader',
					'css-loader'
				]
			},
			{
				test: /\.scss$/,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					'postcss-loader',
					'resolve-url-loader',
					{
						loader: 'sass-loader',
						options: {
							sourceMap: true
						}
					}
				]
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: [
					{
						loader: 'url-loader',
						options: {
							limit: 8192
						}
					}
				]
			}
		]
	}
};
```

### #4 Javascript + Babel and css / sass auto prefixed

Same as configuration #1, #2 and #3 + Auto-prefixer will add vendor prefixes to CSS rules using values from [Can I Use](https://caniuse.com/) to increase browsers compatibility

**Required packages**
*see packages chapter for command lines*

- [Css-loader](https://webpack.js.org/loaders/css-loader/)
- [Sass-loader + Sass](https://webpack.js.org/loaders/sass-loader/)
- [Style-loader](https://webpack.js.org/loaders/style-loader/)
- [Mini-css-extract-plugin](https://webpack.js.org/plugins/mini-css-extract-plugin/)
- [Babel loader](https://github.com/babel/babel)
- [Autoprefixer](https://github.com/postcss/autoprefixer#webpack)
- [Postcss loader](https://www.npmjs.com/package/postcss-loader)
- [File loader](https://webpack.js.org/loaders/file-loader/)
- [Resolve url loader](https://www.npmjs.com/package/resolve-url-loader)

**Required files**

You need to create *postcss.config.js* and edit it

```js
module.exports = {
    plugins: [
      require('autoprefixer')
    ]
  }
```

**webpack.config.js**

```js
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
	mode: 'development',
	// mode: 'production',
	entry: {
		app: "./assets/js/app.js"
	},
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, 'public')
	},
  	plugins: [
	  	new MiniCssExtractPlugin({
	  		filename: '[name].css'
	  	})
  	],
	module: {
		rules: [
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env']
					}
				}
			},
			{
				test: /\.css$/,
				use: [
					'style-loader',
					'css-loader'
				]
			},
			{
				test: /\.scss$/,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					'postcss-loader',
					'resolve-url-loader',
					{
						loader: 'sass-loader',
						options: {
							sourceMap: true
						}
					}
				]
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: [
					{
						loader: 'url-loader',
						options: {
							limit: 8192
						}
					}
				]
			}
		]
	}
};
```

## Packages

[Css-loader](https://webpack.js.org/loaders/css-loader/)

```shell
npm install --save-dev css-loader
```

[Sass-loader + Sass](https://webpack.js.org/loaders/sass-loader/)

```shell
npm install --save-dev sass-loader sass webpack
```

[Style-loader](https://webpack.js.org/loaders/style-loader/)

```shell
npm install --save-dev sass-loader sass
```

[Mini-css-extract-plugin](https://webpack.js.org/plugins/mini-css-extract-plugin/)

```shell
npm install --save-dev mini-css-extract-plugin
```

[Babel loader](https://github.com/babel/babel)

```shell
npm install -D babel-loader @babel/core @babel/preset-env webpack
```

[Autoprefixer](https://github.com/postcss/autoprefixer#webpack)

```shell
npm install --save-dev autoprefixer
```

[Postcss loader](https://www.npmjs.com/package/postcss-loader)

```shell
npm install --save-dev postcss-loader postcss
```

[File loader](https://webpack.js.org/loaders/file-loader/)

```shell
npm install --save-dev file-loader
```

[Resolve url loader](https://www.npmjs.com/package/resolve-url-loader)

```shell
npm install --save-dev resolve-url-loader
```

